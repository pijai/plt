package it.uniba.plt;

public class Translator {
	
	public final static String NIL = "nil";
	
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}
	
	public String getPhrase() {
		return phrase;
	}
	
	public String translate() {
		
		if(startWithVowel()) {
			if (endWithVowel()) {
				return phrase + "yay";
			}
			if(!endWithVowel() && !phrase.endsWith("y")) {
				return phrase + "ay";
			}
			return phrase + "nay";
		} 
		if(!startWithVowel() && !phrase.isEmpty()) {
			int i = 0;
			char carattere = 0;
			while(!startWithVowel()) {
				carattere = phrase.charAt(i);
				phrase = phrase.substring(i+1);
				phrase = phrase + carattere;
				
			}
			return phrase +"ay";
		} 			
		return NIL;
	}
	
	public boolean startWithVowel() {
		return(phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"));
	}
	
	public boolean endWithVowel() {
		return(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	public String translatePhrase() {
		String[] parts;
		String part1;
		String part2;
		if(phrase.contains("-")) {
			parts = phrase.split("-");
			part1 = parts[0];
			part2 = parts[1];
			Translator translator = new Translator(part1);
			part1 = translator.translate();
			Translator translator2 = new Translator(part2);
			part2 = translator2.translate();
			phrase = part1 + "-" + part2;
		}else if(phrase.contains(" ")) {
			parts = phrase.split(" ");
			part1 = parts[0];
			part2 = parts[1];
			Translator translator = new Translator(part1);
			part1 = translator.translate();
			Translator translator2 = new Translator(part2);
			part2 = translator2.translate();
			phrase = part1 + " " + part2;
		}
		return phrase;
	}
}
